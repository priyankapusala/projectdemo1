package com.companydemo1;

public class EmployeeTester {
    public static void main(String[] args)
    {
        Employee employee=new Employee();
        employee.setFirstName("John");
        employee.setLastName("Doe");
        employee.setEmail("John@luv2code.com");
        System.out.println("before calling modifyEmployee() method");
        employee.getDetails();
        Employee employee1=employee.modifyEmployee(employee);
        System.out.println("after calling modifyEmployee() method");
        employee.getDetails();
    }

}
