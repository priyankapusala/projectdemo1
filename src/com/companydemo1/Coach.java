package com.companydemo1;
import java.util.UUID;
public class Coach {
    private String coachName;
    private String coachType;
    private String coachId;
    //constructor chaining
    public Coach() {

        this(UUID.randomUUID().toString());
        System.out.println("hi");
    }

    public Coach(String coachId) {
        this("Anil Kubmle","T-20");
        System.out.println("hi1");
        this.coachId = coachId;
        System.out.println("hello1");


    }

    public Coach(String coachName, String coachType) {
        this(coachName,coachType,UUID.randomUUID().toString());
        System.out.println("hi2");
        this.coachName = coachName;
        this.coachType = coachType;
        System.out.println("hello2");
    }

    public Coach(String coachName, String coachType, String coachId) {
        System.out.println("hi3");
        this.coachName = coachName;
        this.coachType = coachType;
        this.coachId = coachId;
        System.out.println("hello3");
    }

    public String getDetails()
    {
        return "Coach ID: "+coachId+" Coach Name: "+coachName+" Coach Type"+coachType;
    }
}

