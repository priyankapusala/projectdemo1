package com.companydemo1;
    import java.util.UUID;

    public class Employee {
        private String employeeId;
        private String firstName;
        private String lastName;
        private String email;


        public Employee() {
            String[] temp;
            temp = UUID.randomUUID().toString().split("-");
            this.employeeId = temp[0];
        }


        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Employee modifyEmployee(Employee employee) {
            employee = new Employee();
            employee.setFirstName("Marry");
            employee.setLastName("Public");
            employee.setEmail("marry@luv2code.com");
            System.out.println("within modifyEmployee() method...");
            System.out.println("displaying the updated value:");
            System.out.println(employee.getFirstName());
            this.getDetails();
            return employee;
        }

        public void getDetails() {
            System.out.printf("Employee ID: %s First Name: %s Last Name: %s Email: %s", employeeId, firstName, lastName, email);
        }
    }


